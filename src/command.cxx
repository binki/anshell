#include "command.hxx"
#include "command_external.hxx"
#include "command_internal.hxx"
#include "command_set.hxx"

extern "C"
{
#include <stdio.h>
#include <string.h>
}

#include <sstream>
#include <vector>

command::command()
  :
  async(false)
{
}

command *
command::from_components(const std::vector<std::string>& components, short async)
{
  /* The environment overrides associated with this command. */
  std::map<std::string, std::string> mini_env;

  std::string command_name;

  command *command;

  if (!components.size())
    /* Command is a noop. */
    return NULL;

  command_name = components[0];

  if (command_name.find('=') != std::string::npos)
    command = new command_set(command_name);
  else if (command_internal::is(command_name))
    command = new command_internal(command_name, components);
  else
    command = new command_external(command_name, components);

  if (async)
    command->async = true;

  return command;
}

command::~command()
{
}
