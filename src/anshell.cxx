#include "anshell.hxx"
#include "block.hxx"
#include "path.hxx"

extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
}

anshell::anshell()
  :
  cwd(),
  env(environment::from_environ()),
  term(env),
  exit(false)
{
  setcwd();
}

int
anshell::run(int argc, char *argv[])
{
  int opt;
  std::string *input;
  block block;

  /* Ask getopt() to display error messages to the user. */
  opterr = 1;

  while ((opt = getopt(argc, argv, "hv")) != -1)
    switch (opt)
      {
      case 'h':
	printf("%s [-hv] [<script>]\n"
	       PACKAGE_STRING " - An Shell.\n"
	       "\n"
	       " -h Display this help text and exit.\n"
	       " -v Display version information and exit.\n",
	       argv[0]);
	return 0;
	break;

      case 'v':
	printf(PACKAGE_STRING " - An Shell.\n");
	return 0;
	break;
      }

  env.set("PS1", "\\u@\\h:\\w $ ");

  while (!this->exit && (input = term.prompt(*this)))
    {
      block = block::from_input(*input);
      /* If no command is enterred, NULL is returned: */
      block.execute(*this);

      delete input;
    }

  return 0;
}

std::string
anshell::getcwd() const
{
  return cwd;
}

void
anshell::setcwd()
{
  char *cwdbuf;
  size_t cwd_len;

  /* start out with a buffer of the same size as before. */
  cwd_len = cwd.length();
  if (!cwd_len)
    /*
     * Default to a low number so that the functionality of the code
     * below is actually exercised.
     */
    cwd_len = 8;

  cwdbuf = (char *)malloc(cwd_len);
  while (!::getcwd(cwdbuf, cwd_len))
    /* Try a bigger buffer. */
    cwdbuf = (char *)realloc(cwdbuf, cwd_len += 8);

  cwd = cwdbuf;
  free(cwdbuf);
}

const environment&
anshell::environment_get() const
{
  return env;
}

environment&
anshell::environment_get()
{
  return env;
}

int anshell::which(std::string& executable) const
{
  /* figure out the path to the executable */
  if (executable.find('/') == std::string::npos) {
    /* not given an absolute or relative path */
    int index = env.path.find(executable);
    if (index == -1) { return -1; } // didn't find it
    /* we found it, so add the directory on the front for full path */
    executable = env.path.getDirectory(index) + '/' + executable;
  } 
  /* now executable should be qualified to find a file */
  if ( access( executable.c_str() , X_OK ) == 0) {
    struct stat stat_buf;
    if ( lstat(executable.c_str() , &stat_buf ) == -1) {
      /* lstat() should never fail us, but if it were to do so, */
      fprintf(stderr, "anshell::which(): lstat() failed.\n");
    } else { // lstat succeeded
      if ( ! S_ISDIR( stat_buf.st_mode ) ) {
	return 0; // it can be found, is excutable, and not a directory
      } else return 1; // exists, but is a directory
    } // end if lstat() failure
  } // end if executable
  return -1;
}

void
anshell::exit_graceful()
{
  this->exit = true;
}

anshell::~anshell()
{
}
