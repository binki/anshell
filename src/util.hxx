#ifndef _ANSHELL_UTIL_HXX
#define _ANSHELL_UTIL_HXX

#include <string>
#include <sstream>

namespace util
{
  template <class T> std::string to_str(T obj)
  {
    std::stringstream buf;
    buf << obj;
    return buf.str();
  }
};

#endif /* _ANSHELL_UTIL_HXX */
