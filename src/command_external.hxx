#ifndef _ANSHELL_COMMAND_EXTERNAL_HXX
#define _ANSHELL_COMMAND_EXTERNAL_HXX

#include "command.hxx"

#include <string>
#include <vector>

class anshell;

class command_external
  :
  public command
{
public:
  /**
   * \brief
   *   Construct an external command object.
   *
   * \param command
   *   The external command to execute.
   * \param arguments
   *   The list of arguments to pass to this command.
   */
  command_external(std::string command, std::vector<std::string> arguments);

  /**
   * \brief
   *   Implement command::execute().
   */
  virtual int execute(anshell& anshell);

  virtual ~command_external();

private:
  std::string command;
  char **args;
};

#endif /* _ANSHELL_COMMAND_EXTERNAL_HXX */
