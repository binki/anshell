#include "terminal.hxx"
#include "anshell.hxx"

extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#ifdef HAVE_READLINE_HISTORY_H
#include <readline/history.h>
#endif /* HAVE_READLINE_HISTORY_H */
}

bool
terminal::history_initted = false;

terminal::terminal(environment& _env)
:
  _environment(_env)
{
#ifdef HAVE_READLINE_HISTORY_H
  if (!history_initted)
    {
      history_initted = true;
      using_history();
    }
#endif /* HAVE_READLINE_HISTORY_H */
}

std::string *
terminal::prompt(anshell& anshell) const
{
  char *line;
  std::string *line_string;

  char *ps;

  ps = environment::ps_expand(anshell.getcwd(), _environment["PS1"]);

  line = readline(ps);
  free(ps);
  if (!line)
    {
      /* EOF encountered */
      return NULL;
    }

#ifdef HAVE_READLINE_HISTORY_H
  add_history(line);
#endif /* HAVE_READLINE_HISTORY_H */

  line_string = new std::string(line);
  rl_free(line);
  return line_string;
}

terminal::~terminal()
{
#ifdef HAVE_READLINE_HISTORY_H
  if (history_initted)
    {
      clear_history();
    }
#endif /* HAVE_READLINE_HISTORY_H */
}
