#ifndef _ANSHELL_COMMAND_HXX
#define _ANSHELL_COMMAND_HXX

#include "anshell.hxx"

#include <string>
#include <vector>

class anshell;

class command
{
protected:
  /**
   * \brief
   *   Parse a user's input into a command.
   */
  command();

  /**
   * \brief
   *   Expand and then get the list of command parameters.
   *
   * \param anshell
   *   The anshell object to use when expanding the parameters.
   * \return
   *   A vectorfull of parameter strings.
   */
  std::vector<std::string> parameters(anshell& anshell);

  /**
   * \brief
   *   Whether or not this command should be executed asynchronously
   */
  bool async;

public:
  /**
   * \brief
   *   Parse a user's input and return a command.
   *
   * \param components
   *   The components of the command to be executed.
   * \param async
   *   Whether or not the command should be executed asynchronously.
   * \return
   *   The command object representing the user's inputted command.
   */
  static command *from_components(const std::vector<std::string>& components, short async);

  /**
   * \brief
   *   Execute this command.
   *
   * \param
   *   The anshell object.
   * \return
   *   The return value of the command.
   */
  virtual int execute(anshell& anshell) = 0;

  virtual ~command();
};

#endif /* _ANSHELL_COMMAND_HXX */
