#ifndef _ANSHELL_COMMAND_INTERNAL_HXX
#define _ANSHELL_COMMAND_INTERNAL_HXX

#include "command.hxx"

#include <string>

class anshell;

class command_internal
  :
  public command
{
public:
  command_internal(const std::string command, const std::vector<std::string>& arguments);

  virtual int execute(anshell& anshell);

  /**
   * \brief
   *   Tests if a given command is implemented internally.
   *
   * \return
   *   True if this is true.
   */
  static bool is(const std::string& command);

private:
  typedef int(*command_func_t)(anshell& anshell, const std::vector<string>& arguments);

  command_internal::command_func_t command_func;
  std::vector<std::string> arguments;

  static std::map<std::string, command_internal::command_func_t> commands;
  /**
   * \brief
   *   Initialize the commands map.
   */
  static void init();
  static bool initted;
};

#endif /* _ANSHELL_COMMAND_INTERNAL_HXX */
