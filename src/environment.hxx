#ifndef _ANSHELL_ENVIRONMENT_HXX
#define _ANSHELL_ENVIRONMENT_HXX

#include "path.hxx"

#include <map>
#include <string>

class envvar
{
public:
  /* this is required to put something into a map :-/ */
  envvar();

  /* The valid constructor */
  envvar(const std::string& value, short exported = 0);

  void export_var();
  short is_exported() const;
  const std::string& str() const;

  ~envvar();

private:
  std::string value;
  short exported;
};

class environment
{
public:
  /**
   * \brief
   *   Construct an empty environment.
   */
  environment();

  /**
   * \brief
   *   Construct an environment based on a libc environ variable.
   *
   * \param entries
   *   A NULL-terminated array of NULL-terminated C strings.
   */
  environment(const char **entries);

  /**
   * \brief
   *   Determine if a value is in the environment.
   */
  bool has(const std::string& var) const;

  /**
   * \brief
   *   Get a value in the environment.
   *
   * \return
   *   The value or an empty string if it is unset.
   */
  const std::string& operator[](const std::string& var) const;

  /**
   * \brief
   *   Set an item of the environment.
   *
   * \param var
   *   The variable to set.
   * \param value
   *   The value to set the variable to.
   */
  void set(const std::string& var, const std::string& value);

  /**
   * \brief
   *   Mark a variable as exported.
   *
   * \param var
   *   The variable to mark.
   */
  void export_var(const std::string& var);

  /**
   * \brief
   *   Unset a variable.
   *
   * \param var
   *   The variable to unset.
   */
  void unset(const std::string& var);

  /**
   * \brief
   *   Pull data from the real environment into the environment.
   *
   * \return
   *   The environment.
   */
  static environment from_environ();

  /**
   * \brief
   *   Convert an environment to an environ.
   *
   * \param include_unexported
   *   Whether or not to include all variable regardless of whether or
   *   not they are exported.
   * \return
   *   The environ. You must free the returned pointer using free().
   */
  char **to_environ(short include_unexported = 0) const;

  /**
   * \brief
   *   Get the current hostname.
   *
   * \return
   *   The hostname as a string which must be free()d.
   */
  static char *gethostname();

  /**
   * \brief
   *   Expand a parameter following the PS1 syntax.
   *
   * \param cwd
   *   The current working directory to be used when expanding param.
   * \param param
   *   The text of the parameter.
   * \return
   *   The expansion, which must be free()d with free()..
   */
  static char *ps_expand(std::string cwd, std::string param);

  /**
   * \brief
   *   The path object which lets external commands be resolved to
   *   files.
   */
  ::path path;

  ~environment();

private:
  /**
   * \brief
   *   A string used when a given variable is accessed by not
   *   found. I.e., the mepty string.
   */
  static const std::string default_string;

  /**
   * \brief
   *   The type used for the entries variable.
   */
  typedef std::map<std::string, envvar> entries_t;

  /**
   * \brief
   *   The values in the environment keyed by envvar name.
   */
  entries_t entries;
};

#endif /* _ANSHELL_ENVIRONMENT_HXX */
