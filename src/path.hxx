#ifndef _ANSHELL_PATH_HXX
#define _ANSHELL_PATH_HXX

#include <vector>
#include <string>
using namespace std;

class path
{
public:
  path(); /* default constructor */
  ~path(); /* destructor */
  void setPathFromString(string pathString);
  size_t find(const string& program) const; /* find the dir to program */
  string getDirectory(size_t i) const; /* directory at index i */
  string catPath() const;
private:
  vector<string> pathsVec;
}; 

#endif /* _ANSHELL_PATH_HXX */
