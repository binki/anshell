#include "environment.hxx"

#include <stdexcept>

extern "C"
{
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
}

/*
 * The illogical constructor forced upon us by our need to store
 * envvars in a std::map.
 */
envvar::envvar()
:
  value(),
  exported(0)
{
}

envvar::envvar(const std::string& _value, short _exported)
  :
  value(_value),
  exported(_exported)
{
}

void
envvar::export_var()
{
  exported = 1;
}

short
envvar::is_exported() const
{
  return exported;
}

const std::string&
envvar::str() const
{
  return value;
}

envvar::~envvar()
{
}

environment::environment()
  :
  path(),
  entries()
{
}

environment::environment(const char **_entries)
  :
  path(),
  entries()
{
  const char *value;
  char *name;
  size_t name_size = 0;

  name = NULL;

  for (; *_entries; _entries ++)
    {
      value = strchr(*_entries, '=');
      if (!value)
	throw std::runtime_error("Invalidly formatted environment entry: `" + std::string(*_entries) + "'.");

      /* grab the envvar's name */
      if (name_size < (size_t)(value - *_entries))
	{
	  name_size = value - *_entries;
	  free(name);
	  name = (char *)malloc(name_size + 1);
	  if (!name)
	    throw std::runtime_error("Out of memory.");
	}
      memcpy(name, *_entries, value - *_entries);
      name[value - *_entries] = '\0';

      /* skip over the '=' in the environ entry */
      value ++;

      entries[std::string(name)] = envvar(std::string(value), 1);
    }

  if (has("PATH"))
    path.setPathFromString((*this)["PATH"]);

  free(name);
}

environment
environment::from_environ()
{
  return environment((const char**)environ);
}

bool
environment::has(const std::string& var) const
{
  return entries.find(var) != entries.end();
}

const std::string
environment::default_string = "";

const std::string&
environment::operator[](const std::string& var) const
{
  if (!has(var))
    return environment::default_string;
  return entries.find(var)->second.str();
}

void
environment::set(const std::string& var, const std::string& value)
{
  entries[var] = value;

  /* side-effects */
  if (var == "PATH")
    path.setPathFromString(value);
}

void
environment::export_var(const std::string& var)
{
  if (! has(var)) return;
  entries[var].export_var();
}

void
environment::unset(const std::string& var)
{
  if (has(var))
    entries.erase(entries.find(var));
}

char **
environment::to_environ(short include_unexported) const
{
  size_t environ_len;
  entries_t::const_iterator i;
  char **_environ;

  size_t environ_i;
  char *environ_entry;

  environ_len = sizeof(char *) * (entries.size() + 1 /* NULL array terminator */);
  for (i = entries.begin(); i != entries.end(); i ++)
    if (i->second.is_exported() || include_unexported)
      environ_len += i->first.length() + 1 /* = */ + i->second.str().length() + 1 /* '\0' */;

  _environ = (char **)malloc(environ_len);
  if (!_environ)
    throw std::runtime_error("Out of memory.");

  environ_entry = (char *)((intptr_t)_environ) + sizeof(char *) * (entries.size() + 1);
  for (i = entries.begin(), environ_i = 0;
       i != entries.end();
       i ++)
    {
      /* Don't encode silly-named envvars. */
      if (!i->first.length() || strchr("?!", i->first[0]))
	  continue;

      if (!i->second.is_exported() && !include_unexported)
	continue;

      _environ[environ_i] = environ_entry;

      memcpy(environ_entry, i->first.c_str(), i->first.length());
      environ_entry += i->first.length();

      *environ_entry = '=';
      environ_entry ++;

      memcpy(environ_entry, i->second.str().c_str(), i->second.str().length());
      environ_entry += i->second.str().length();

      *environ_entry = '\0';
      environ_entry ++;
      environ_i ++;
    }
  /* NULL-terminate the array part */
  _environ[environ_i] = NULL;

  return _environ;
}

char *
environment::gethostname()
{
  char *hostname;
  size_t hostname_max_len;

  hostname_max_len = sysconf(_SC_HOST_NAME_MAX);
  hostname = (char *)malloc(hostname_max_len);
  ::gethostname(hostname, hostname_max_len);

  /* Ensure hostname is NULL-terminated */
  hostname[hostname_max_len - 1] = '\0';

  return hostname;
}

char *
environment::ps_expand(std::string cwd, std::string param)
{
  char *hostname;
  char *username;

  /* The expanded string */
  char *ps;
  char *ps_ptr;
  char *tmp;

  size_t ps_len;
  size_t param_i;

  hostname = NULL;
  username = NULL;

  ps_len = 1 /* NULL terminator */;
  for (param_i = 0; param_i < param.length(); param_i ++)
    {
      if (param[param_i] == '\\'
	  && param_i + 1 < param.length())
	{
	  /* skip the '\\' char */
	  param_i ++;

	  /* Figure out how many bytes this escape sequence consumes */
	  switch (param[param_i])
	    {
	    case 'h':
	      if (!hostname)
		hostname = environment::gethostname();
	      ps_len += strlen(hostname);
	      break;

	    case 'u':
	      if (!username)
		{
		  tmp = getlogin();
		  if (tmp)
		    username = strdup(getlogin());
		  else
		    username = strdup("<unknown>");
		}

	      ps_len += strlen(username);
	      break;

	    case 'w':
	      ps_len += cwd.length();
	      break;
	    }
	}
      else
	ps_len ++;
    }

  ps = (char *)malloc(ps_len);
  ps_ptr = ps;

  /* Build the new string */
  for (param_i = 0; param_i < param.length(); param_i ++)
    {
      if (param[param_i] == '\\'
	  && param_i + 1 < param.length())
	{
	  /* skip the '\\' char */
	  param_i ++;

	  switch (param[param_i])
	    {
	    case 'h':
	      memcpy(ps_ptr, hostname, strlen(hostname));
	      ps_ptr += strlen(hostname);
	      break;

	    case 'u':
	      memcpy(ps_ptr, username, strlen(username));
	      ps_ptr += strlen(username);
	      break;

	    case 'w':
	      memcpy(ps_ptr, cwd.c_str(), cwd.length());
	      ps_ptr += cwd.length();
	      break;
	    }
	}
      else
	{
	  *ps_ptr = param[param_i];
	  ps_ptr ++;
	}
    }
  *ps_ptr = '\0';

  free(hostname);
  free(username);

  return ps;
}

environment::~environment()
{
}
