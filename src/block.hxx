#ifndef _ANSHELL_BLOCK_HXX
#define _ANSHELL_BLOCK_HXX

#include <string>
#include <vector>
#include "anshell.hxx"

class command;

/**
 * \brief
 *   Internal representation of an un-expanded command.
 */
class block_element
{
public:
  /**
   * \brief
   *   Construct a block_element based on a list of tokens.
   *
   * \param tokens
   *   The tokens which make up the command which will be expanded
   *   when this command element is to be executed.
   * \param async
   *   Whether or not the command is specified to be asynchronous. To
   *   be correct, the whole built block should be marked
   *   asynchronous... but baby steps...
   */
  block_element(std::vector<std::string> tokens, short async);

  /**
   * \brief
   *   Construct an appropriate command object after expansion of
   *   tokens.
   *
   * \param anshell
   *   The anshell object to use when performing shell expansion.
   * \return
   *   The command object.
   */
  command *expand(anshell& anshell) const;

private:
  std::vector<std::string> tokens;
  short async;
};

/**
 * \brief
 *   A block of commands.
 *
 * Multiple commands which are grouped together and are to be executed
 * sequentially. Each of these commands is not resolved to a command
 * object until it is the command's turn to be executed. This way,
 * variable substitution will work as expected in the case where
 * variables are altered.
 */
class block
{
protected:
  block(std::vector<block_element> elements);

public:
  /**
   * \brief
   *   A NULL constructor for an empty block.
   */
  block();

  /**
   * \brief
   *   Execute this block of commands.
   *
   * \param anshell
   *   An anshell insance to use in parameter expansion and
   *   environmental care.
   * \return
   *   The return value of the commands.
   */
  int execute(anshell& anshell);

  /**
   * \brief
   *   Parse a line of input into a command block.
   *
   * \param input
   *   A line of the user's input.
   */
  static block from_input(const std::string& input);

private:
  std::vector<block_element> elements;
};

#endif /* _ANSHELL_BLOCK_HXX */
