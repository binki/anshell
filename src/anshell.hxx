#ifndef _ANSHELL_ANSHELL_HXX
#define _ANSHELL_ANSHELL_HXX

#include "environment.hxx"
#include "terminal.hxx"
#include "path.hxx"

class anshell
{
public:
  anshell();
  virtual int run(int argc, char *argv[]);

  /**
   * \brief
   *   Get the current working directory of the shell.
   *
   * \return
   *   The current working directory.
   */
  std::string getcwd() const;

  /**
   * \brief
   *   Get a copy of the environment.
   */
  environment& environment_get();
  const environment& environment_get() const;
  
  /**
   * \brief
   *   Search the path for an executable given its name.
   * \return
   *   -1 if not found, 0 otherwise
   */
  int which(std::string& executable) const;

  /**
   * \brief
   *   Update the cached cwd variable.
   *
   * Must be called when cd is invoked and when anshell is
   * instantiated.
   */
  void setcwd();

  /**
   * \brief
   *   Initiate a graceful exit.
   */
  void exit_graceful();

  ~anshell();

private:
  /**
   * \brief
   *   The getcwd() call is expensive to make, so we cache its value here.
   */
  std::string cwd;

  environment env;
  terminal term;

  /* whether or not to exit on the next loop in anshell::run() */
  bool exit;
};

#endif /* _ANSHELL_ANSHELL_HXX */
