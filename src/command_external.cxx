#include "command_external.hxx"
#include "path.hxx"
#include "util.hxx"

#include <iostream>

using namespace std;
extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
}

command_external::command_external(std::string _command, std::vector<std::string> arguments)
:
  command(_command),
  args(NULL)
{
  std::vector<std::string>::const_iterator arguments_iter;
  size_t args_i;

  args = (char **)malloc(sizeof(char *) * (arguments.size() + 1));
  if (!args)
    fprintf(stderr, "OOM!\n");

  for (arguments_iter = arguments.begin(), args_i = 0;
       arguments_iter != arguments.end();
       arguments_iter ++, args_i ++)
    args[args_i] = strdup(arguments_iter->c_str());
  args[args_i] = NULL;
}

int
command_external::execute(anshell& anshell)
{
  int stat_loc;
  int which_info = anshell.which(command);
  if ( which_info < 0) {
    cerr << "anshell: " << command << ": command not found" << endl;
    return -1;
  } else if (which_info > 0) {
    cerr << "anshell: " << command << ": is a directory" << endl;
  }// which() returned 0, command is now different

  pid_t pid = fork();
  if (pid == 0) {
    /* child must become the new target process */
    execve(command.c_str(), // fully qualified file name
	   args, // pass args so that the new argv[0] is the commmand
	   anshell.environment_get().to_environ()); // load environment
    exit(0);
  } else if (pid < 0)
    {
      cerr << "Fork failed!" << endl;
      anshell.environment_get().set("?", "1");
      return 1;
    }
  else {
    /* the parent must wait unless if this is an asynchronous command */
    if (async)
      {
	string pid_str(util::to_str(pid));
	printf("anshell: spawned process %s\n", pid_str.c_str());
	anshell.environment_get().set("!", pid_str);
	anshell.environment_get().set("?", "0");
	return 0;
      }
    else
      {
	waitpid(pid, &stat_loc, 0);
	if (!stat_loc)
	  stat_loc = 0;
	else
	  stat_loc = WEXITSTATUS(stat_loc);
	anshell.environment_get().set("?", util::to_str(stat_loc));
	return stat_loc;
      }
  }

  /* unreachable */
  return -1;
}

command_external::~command_external()
{
  size_t args_i;

  for (args_i = 0; args[args_i]; args_i ++)
    free(args[args_i]);
  free(args);
}
