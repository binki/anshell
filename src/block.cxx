#include "block.hxx"
#include "command.hxx"

extern "C"
{
#include <stdio.h>
#include <string.h>
#include <unistd.h>
}

#include <sstream>
#include <string>
#include <vector>

/* local static declarations */
static size_t command_token_parse(anshell *anshell, std::string& token, const std::string& line, size_t pos, short& null);
static size_t command_singlequote_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos);
static size_t command_doublequote_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos);
static size_t command_backslash_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos, short double_quotes);
static size_t command_parameter_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos);
static size_t command_parameter_variable_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos);

/*
 * The set of procedural input parsing/expansion functions. Sorry, I
 * could not think of an object-oriented way to solve this problem of
 * parameter expansion/preparsing.
 */

/**
 * \brief
 *   Eat up a token.
 *
 * \param anshell
 *   If this parameter is NOT NULL, then the tokens will undergo
 *   full-fledged shell substitution.
 * \param token
 *   Where to deposit the tokens.
 * \param line
 *   The input line.
 * \param pos
 *   The position on the line.
 * \param null
 *   Set to nonzero if we parsed a ``null'' token which should be
 *   discarded and not even treated as an empty string.
 * \return
 *   The number of chars in the input line I ate. 0 means we're done
 *   with the input.
 */
static size_t command_token_parse(anshell *anshell, std::string& token, const std::string& line, size_t pos, short& null)
{
  size_t i;
  std::stringbuf tokenbuf;
  size_t tmp;

  null = 0;

  for (i = pos;
       i < line.length() && strchr(" \t\n", line[i]);
       i ++)
    ;

  if (line[i] == '#')
    {
      null = 1;
      return 0;
    }

  /* Avoid generating an empty token at the end of every line: */
  if (i >= line.length())
    return 0;

  while (i < line.length())
    {
      switch (line[i])
	{
	case '\'':
	  i += command_singlequote_parse(anshell, tokenbuf, line, i);
	  break;

	case '"':
	  i += command_doublequote_parse(anshell, tokenbuf, line, i);
	  break;

	case '\\':
	  i += command_backslash_parse(anshell, tokenbuf, line, i, 0);
	  break;

	case ' ':
	case '\t':
	case '\n':
	  /* Be done with this token */
	  token = tokenbuf.str();
	  return i - pos;

	case '$':
	  i += command_parameter_parse(anshell, tokenbuf, line, i);
	  break;

	  /*
	   * These chars have to act as tokens on their own because users like to do things like:
	   *
	   * echo; echo;
	   *
	   * instead of
	   *
	   * echo ; echo
	   */
	case ';':
	case '&':
	  if (i - pos)
	    {
	      /*
	       * We're finishing up a previous token, leave these
	       * chars for next time... but check if they're a null
	       * token first ;-).
	       */
	      token = tokenbuf.str();
	      for (tmp = 0;
		   tmp < token.length() && strchr(" \t\n", token[tmp]);
		   tmp ++)
		;
	      if (tmp >= token.length())
		null = 1;
	      return i - pos;
	    }
	  /* We haven't read anything else yet, so throw these into the buffer and get movin' */
	  token = line[i];
	  i ++;
	  return i - pos;

	default:
	  tokenbuf.sputc(line[i]);
	  i ++;
	  break;
	}
    }
  token = tokenbuf.str();
  return i - pos;
}

/**
 * \brief
 *   Parse a single-quoted string.
 *
 * \param anshell
 *   Not NULL if we are actually performing parameter expansion.
 * \param token
 *   The token to append chars to.
 * \param line
 *   The line being parsed.
 * \param pos
 *   The position in the line being parsed.
 * \return
 *   The number of characters consumed.
 */
static size_t command_singlequote_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos)
{
  size_t parsed_chars;

  /* hop over the openning singlequote */
  parsed_chars = 1;
  if (!anshell)
    /* but preserve it when not doing parameter expansion */
    token.sputc(line[pos]);
  while (line[pos + parsed_chars] != '\'')
    {
      token.sputc(line[pos + parsed_chars]);
      parsed_chars ++;
    }
  /* hop over closing singlequote unless we're not doing parameter expansion */
  if (!anshell)
    token.sputc(line[pos + parsed_chars]);
  parsed_chars ++;

  return parsed_chars;
}

/**
 * \brief
 *   Parse a double-quoted block of text.
 *
 * \param anshell
 *   The anshell context used for parameter expansion.
 * \param token
 *   The buffer to append accepted chars to.
 * \param line
 *   The line being parsed.
 * \param pos
 *   The position in the line being parsed.
 * \return
 *   The number of eaten chars.
 */
static size_t command_doublequote_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos)
{
  size_t parsed_chars;

  if (!anshell)
    token.sputc(line[pos]);
  parsed_chars = 1;
  while (line[pos + parsed_chars] != '"')
    {
      switch (line[pos + parsed_chars])
	{
	case '\\':
	  parsed_chars += command_backslash_parse(anshell, token, line, pos + parsed_chars, 1);
	  break;

	case '$':
	  parsed_chars += command_parameter_parse(anshell, token, line, pos + parsed_chars);
	  break;

	default:
	  if (!anshell)
	    token.sputc(line[pos + parsed_chars]);
	  if (anshell)
	    token.sputc(line[pos + parsed_chars]);
	  parsed_chars ++;
	  break;
	}
    }
  if (!anshell)
    token.sputc(line[pos + parsed_chars]);
  /* hop over closing singlequote */
  parsed_chars ++;

  return parsed_chars;
}

/**
 * \brief
 *   Parse a basic backslash-escaped character.
 *
 * \param anshell
 *   Whether or not we are expanding or parsing.
 * \param token
 *   Where to append results.
 * \param line
 *   The line we're reading.
 * \param pos
 *   The current position.
 * \param double_quotes
 *   Whether or not we're in a double-quoted string because "\d" -> \d
 *   and \d -> d.
 */
static size_t command_backslash_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos, short double_quotes)
{
  size_t i;

  i = pos;

  if (!anshell)
    token.sputc('\\');
  i ++;

  if (i >= line.length())
    {
      if (anshell)
	token.sputc('\\');
      return 1;
    }

  /* under double-quotes, we output the backslash _if_ we don't recognize the char. */
  if (anshell
      && double_quotes
      && !strchr("\"$\\", line[i]))
    token.sputc('\\');

  token.sputc(line[i]);

  return 2;
}

/**
 * \brief
 *   Perform a parameter expansion.
 *
 * \return
 *   The number of chars consumed in the expansion.
 */
static size_t command_parameter_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos)
{
  size_t i;

  i = pos;

  if (!anshell)
    token.sputc(line[i]);
  /* jump the `$' */
  i ++;

  switch (line[i])
    {
    case '{':
    default:
      i += command_parameter_variable_parse(anshell, token, line, i);
      break;
    }

  return i - pos;
}

/**
 * \brief
 *   Expand an environment variable.
 *
 * \param anshell
 *   Place to get variable from, NULL if we're only parsing and not
 *   expanding.
 * \param token
 *   Where to append results to.
 * \param line
 *   Line being parsed
 * \param pos
 *   Position in the line being parsed
 */
static size_t command_parameter_variable_parse(anshell *anshell, std::stringbuf& token, const std::string& line, size_t pos)
{
  size_t i;
  short braced;
  std::stringbuf variable_name;
  std::string value;

  i = pos;
  braced = 0;
  if (line[i] == '{')
    {
      braced = 1;
      if (!anshell)
	token.sputc(line[i]);
      i ++;
    }

  /* one-char long variables */
  if (strchr("0123456789!?@", line[i]))
    {
      if (anshell)
	variable_name.sputc(line[i]);
      else
	token.sputc(line[i]);
      i ++;
    }
  else
    {
      /*
       * Multi-char variable names.
       *
       * Numbers which don't start a variable may be part of a bigger
       * variable name, so they're valid here (we already know that
       * the first char of the variable will not be a numeral).
       */
      for (; i < line.length() && strchr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_123456890", line[i]);
	   i ++)
	if (anshell)
	  variable_name.sputc(line[i]);
	else
	  token.sputc(line[i]);
    }

  /* for now, if a variable is braced just parse 'til the end of the ${} thing */
  if (braced)
    {
      for (; i < line.length() && line[i] != '}'; i ++)
	if (anshell)
	  variable_name.sputc(line[i]);
	else
	  token.sputc(line[i]);

      /* either there are no more chars or there is a `}' */
      if (i < line.length())
	{
	  /* then we hit a `}' and must skip over it */
	  if (!anshell)
	    token.sputc(line[i]);
	  /* jump over the `}' */
	  i ++;
	}
    }

  if (anshell && anshell->environment_get().has(variable_name.str()))
    {
      value = anshell->environment_get()[variable_name.str()];
      token.sputn(value.c_str(), value.length());
    }

  return i - pos;
}

block_element::block_element(std::vector<std::string> _tokens, short _async)
 :
  tokens(_tokens),
  async(_async)
{
}

command *
block_element::expand(anshell& anshell) const
{
  std::vector<std::string> command_components;
  std::string expanded_component;
  std::vector<std::string>::const_iterator token;
  short null;

  for (token = tokens.begin(); token != tokens.end(); token ++)
    {
      command_token_parse(&anshell, expanded_component, *token, 0, null);
      command_components.push_back(expanded_component);
    }

  return command::from_components(command_components, async);
}

block
block::from_input(const std::string& input)
{
  size_t pos;
  size_t tmp;

  std::string token;
  std::vector<std::string> components;
  std::vector<block_element> elements;

  short async;
  short end;
  short null;

  /* Break into pieces */
  async = 0;
  pos = 0;
  null = 1;
  while ((tmp = command_token_parse(NULL, token, input, pos, null)))
    {
      async = 0;
      end = 0;

      do
	{
	  pos += tmp;

	  /* Test for the end of a command. */
	  if (!strcmp(token.c_str(), "&"))
	    {
	      async = 1;
	      end = 1;
	    }
	  if (!strcmp(token.c_str(), ";"))
	    /* The end is near... */
	    end = 1;

	  if (end)
	    {
	      elements.push_back(block_element(components, async));
	      components.clear();
	      break;
	    }

	  /*
	   * Add the parsed token to the list of command components
	   * (and thus exclude friends like & and ; when doing so ;-).
	   */
	  if (!null)
	    components.push_back(token);
	}
      while ((tmp = command_token_parse(NULL, token, input, pos, null)));
    }

  /* Most commands are not suffixed with a `;' character, so catch those here */
  if (components.size())
    elements.push_back(block_element(components, async));

  return block(elements);
}

block::block(std::vector<block_element> _elements)
  :
  elements(_elements)
{
}

block::block()
  :
  elements()
{
}

int
block::execute(anshell& anshell)
{
  std::vector<block_element>::const_iterator i;
  command *expanded_command;
  int ret;

  ret = 0;

  for (i = elements.begin(); i != elements.end(); i ++)
    {
      expanded_command = i->expand(anshell);
      if (expanded_command)
	{
	  ret = expanded_command->execute(anshell);
	  delete expanded_command;
	}
    }

  return ret;
}
