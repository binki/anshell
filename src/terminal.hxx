#ifndef _ANSHELL_TERMINAL_HXX
#define _ANSHELL_TERMINAL_HXX

#include "environment.hxx"

class anshell;

class terminal
{
private:
  environment& _environment;

public:
  /**
   * \brief
   *   Construct a new terminal object.
   *
   * \param environment
   *   The environment object from which the prompt should be
   *   constructed.
   */
  terminal(environment& environment);

  /**
   * \brief
   *   Present the user with a prompt and return the enterred comand.
   *
   * \param anshell
   *   The global anshell object.
   *
   * \return
   *   The command the user typed as a string or NULL at EOF. The
   *   returned std::string must be deleted.
   */
  std::string *prompt(anshell& anshell) const;

  ~terminal();

private:
  static bool history_initted;
};

#endif /* _ANSHELL_TERMINAL_HXX */
