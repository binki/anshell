#include "command_set.hxx"
#include "anshell.hxx"

#include <stdexcept>

command_set::command_set(const std::string& assignment)
  :
  var(),
  value()
{
  size_t equal_i;

  equal_i = assignment.find('=');
  if (equal_i == std::string::npos)
    throw runtime_error("Invalid assignment: " + assignment);

  var = assignment.substr(0, equal_i);
  value = assignment.substr(equal_i + 1);
}

int
command_set::execute(anshell& anshell)
{
  anshell.environment_get().set(var, value);
  return 0;
}
