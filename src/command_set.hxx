#ifndef _ANSHELL_COMMAND_SET_HXX
#define _ANSHELL_COMMAND_SET_HXX

#include "command.hxx"

class anshell;

/**
 * \brief
 *   A command which consists of a variable assignment.
 */
class command_set
  :
  public command
{
public:
  /**
   * \brief
   *   Generate a command_set object.
   *
   * \param assignment
   *   An assignment formatted like A=sdfdsf.
   */
  command_set(const std::string& assignment);

  virtual int execute(anshell& anshell);

private:
  std::string var;
  std::string value;
};

#endif /* _ANSHELL_COMMAND_SET_HXX */
