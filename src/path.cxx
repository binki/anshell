#include "path.hxx"
/* anshell path implementation */

/* class requirements */
#include <vector>  // for vector
#include <string>  // for string
#include <stdio.h> // for printf, stdout, stderr
#include <unistd.h> // for sleep, DEBUG FEATURE

/* for path() */
#include <cstdlib> // for getenv()

/* for find() */
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

/* for getDirectory */

using namespace std;

/*** path(): the anshell path default constructor **********************
 * Written by Avery Sterk 
 ***********************************************************************
 * Receives: <void>
 *  Returns: a new path object
 * Behavior: looks up the user's PATH variable and imports it into
 *           a private vector of strings.
 * Assumptions: the user is making a path when the PATH variable is set.
 ***********************************************************************/
path::path()
{ /* default constructor */
  /* do nothing; wait for someone to give us a path */
}

void path::setPathFromString( string pathString) { // yes, make a copy.
  pathsVec.empty();
  size_t dir_length = pathString.find(':');
  string dir_string;
  while ( dir_length != string::npos) { // while there are still :'s

    // add the string to the vector
    dir_string = pathString.substr(0,dir_length);
    if (pathsVec.size() == 0) {
      pathsVec.push_back( dir_string ); // to just before :
    } else if (dir_string == pathsVec.back() ) { // if it's a repeat,
      fprintf( stderr, "path::setPathFromString(): already have %s\n",
	       dir_string.c_str() );
    } else { // it's not a repeat
      pathsVec.push_back( dir_string );
    }

    // clear the directory
    pathString.erase(0,dir_length+1); // erase through the :
    dir_length = pathString.find(':');
  } // endwhile

  if (pathString.length() > 0)
    pathsVec.push_back(pathString);

}

path::~path() { /* no pointers, so do nothing! */ }

/*** path::find(): the anshell path program lookup *********************
 * Written by Avery Sterk 
 ***********************************************************************
 * Receives: (const string&) program
 *  Returns: (int) giving the index in pathsvec of the directory where
 *           the requested program can by found
 * Behavior: opens the directories in pathsVec until it finds where the
 *           program is located
 * Assumptions: <TBD>
 ***********************************************************************/
size_t
path::find(const string& program) const
{ /* find the dir to program */
  string fq_program; // qualified enough to be found
  DIR * testdir;
  struct dirent *dent;
  size_t i;

  for (i = 0; i < pathsVec.size(); i++) {
    // open
    testdir = opendir(pathsVec[i].c_str());

    // check open
    if (testdir == NULL)
      /* Just skip it silently like normal shells do ;-) */
      continue;

    // check contents
    while ((dent = readdir(testdir))) {
      if (program == (dent -> d_name)) {
	fq_program = pathsVec[i] + '/' + program;
        // on hit: check executable permissions
	if ( access( fq_program.c_str() , X_OK ) == 0) {
	  // if we can execute, check if it is a directory
	  struct stat stat_buf;
	  if ( lstat(fq_program.c_str() , &stat_buf ) == -1) {
	    /* lstat() should never fail us, but if it were to do so, */
	    fprintf( stderr, "anshell::which(): lstat() failed.\n");
	  } else { // lstat succeeded
	    if ( ! S_ISDIR( stat_buf.st_mode ) ) {
	      closedir(testdir);
	      return i; // it can be found, is excutable, and not a directory
	    } // end if it isn't a directory
	  } // end checking with lstat()
	} // if it was executable
      } // end if we found the file
    } // end while we have more things in the directory to check
    // no success: close
    closedir(testdir);
  } // end while we have more in the path
  return -1; // didn't find anything
}

string path::catPath() const
{
  string pathString;
  for (size_t i = 0; i < pathsVec.size(); i++ ) {
    if (i > 0)
      pathString += ':';
    pathString += pathsVec[i];
  }
  return pathString;
}

/*** path::getDirectory(): the anshell path path-list tool *************
 * Written by Avery Sterk 
 ***********************************************************************
 * Receives: (int) i
 *  Returns: (string) containing the name of the i'th directory in PATH
 *           *** 0-indexed.
 *  SPECIAL: - negative i will get the last element in the path
 *           - i > number of directories in path gets "Out of bounds!"
 * Behavior: consults the sign of i, compares i with the number of
 *           directories in the path, then returns as above.
 *           
 * Assumptions: PATH exists
 ***********************************************************************/
std::string
path::getDirectory(size_t i) const
{ /* directory at index i */
  if (i > pathsVec.size()) // unsigned comparison: negatives are true
    return ""; 
  else return pathsVec[i];
}
