#include "command_internal.hxx"
#include "anshell.hxx"
#include "environment.hxx"

extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
}

#include <string>

std::map<std::string, command_internal::command_func_t>
command_internal::commands = std::map<std::string, command_internal::command_func_t>();

bool
command_internal::initted = false;

command_internal::command_internal(const std::string _command, const std::vector<std::string>& _arguments)
:
  command_func(NULL),
  arguments(_arguments)
{
  if (!command_internal::initted)
    init();

  /* discard the first argument which is the command itself */
  arguments.erase(arguments.begin());

  command_func = commands[_command];
}

int
command_internal::execute(anshell& anshell)
{
  return (*command_func)(anshell, arguments);
}

bool
command_internal::is(const std::string& command)
{
  if (!command_internal::initted)
    init();

  return commands.find(command) != commands.end();
}

static int command_internal_help(anshell& anshell, const std::vector<std::string>& arguments)
{
  printf("cd     - Change the current directory.\n"
	 "exit   - Exit the shell.\n"
	 "export - Make a variable accessible by child processes.\n"
	 "false  - Return nonzero.\n"
	 "help   - Display this help.\n"
	 "path   - Print the path as used by the shell.\n"
	 "pwd    - Display the current directory.\n"
	 "type   - Tell where a command is defined.\n"
	 "true   - Return zero.\n");
  return 0;
}

/* pwd, written by Avery Sterk */
static int command_internal_pwd(anshell& anshell, const std::vector<std::string>& arguments)
{
  //  if ( anshell.environment_get().entries.find("PWD") == 
  //       anshell.environment_get().entries.end() ) {
  if ( anshell.environment_get().has("PWD") )
    {  /* the environment does not have PWD set */
      size_t buf_size(256);
      char* cwd_buf;
      cwd_buf = getcwd(cwd_buf, buf_size); // try to fit into 256 characters
      while(cwd_buf == NULL) { // until it fits,
	buf_size *= 2; // double the allowable length
	cwd_buf = getcwd(cwd_buf, buf_size); // try again
      } // end while -- must now have it in cwd_buf.
      printf("%s\n", cwd_buf);
      free(cwd_buf); // getcwd malloc's, so we must make it free
      return 0;
    } else {
    /* PWD is set by the environment, so use it */
    string env_pwd(anshell.environment_get()["PWD"]);
    printf("%s\n", env_pwd.c_str() );
    return 0;
  } // end if
  /* unreachable */
  return -1;
}


/* cd, written by Avery Sterk */
/* with no argument: tries to cd into $HOME,
 * with an argument: tries to cd into its first argument */
static int command_internal_cd(anshell& anshell, const std::vector<std::string>& arguments)
{
  environment anshell_env = anshell.environment_get(); // by ref
  if ( arguments.size() == 0) {
    /* go home */
    if ( anshell.environment_get().has("HOME") ) {
      string homedir = anshell.environment_get()["HOME"];
      int hold_this = chdir( homedir.c_str() );
      if ( hold_this == 0 ) {
	if ( anshell.environment_get().has("PWD") ) { // update OLDPWD
	  anshell_env.set("OLDPWD", anshell_env["PWD"]);
	}
	// update PWD
	anshell_env.set("PWD", homedir);
	anshell.setcwd(); // update anshell's internal cwd
      } else {
	fprintf(stderr, "anshell: cd: failed to change directories");
      }
      return hold_this;
    } else {
      fprintf(stderr, "anshell: cd: HOME is not set.");
      return -1;
    }
  } else {
    if ( chdir( arguments[0].c_str() ) == 0)
    { /* success in changing directories */
      if ( anshell.environment_get().has("PWD") ) {
	anshell_env.set("OLDPWD", anshell_env["PWD"]);
      }
      anshell.setcwd();
      return 0;
    } else { /* failure in changing directories */
      fprintf(stderr, "anshell: cd: failed to change directories.");
      return -1;
    }
    return 0;
  }
}

static int command_internal_exit(anshell& anshell, const std::vector<std::string>& arguments)
{
  anshell.exit_graceful();
  return 0;
}

/* type - displays where commands are defined */
static int command_internal_type(anshell& anshell, const std::vector<std::string>& arguments)
{
  std::string cmd;
  for (size_t i = 0; i < arguments.size(); i++ ) {
    cmd = arguments[i]; // cache locally by copying
    if ( command_internal::is( cmd ) ) {
      printf("%s is internal to anshell\n", cmd.c_str() );
    } else {
      int which_info = anshell.which( cmd );
      if ( which_info == 0 ) { // can find in path
	printf( "%s is external to anshell: %s\n",
		arguments[i].c_str(), cmd.c_str() );
      } else { // cannot find in path
	fprintf( stderr, "anshell: type: %s: command not found\n", cmd.c_str());
      }
    }
  } // else do nothing
  return 0;
}

/* path, written by Avery Sterk */
static int command_internal_path(anshell& anshell, const std::vector<std::string>& arguments)
{
  string path_string(anshell.environment_get().path.catPath());
  printf( "%s\n", path_string.c_str() );
  return 0;
}

static int command_internal_true(anshell& anshell, const std::vector<std::string>& arguments)
{
  anshell.environment_get().set("?","0");
  return 0;
}

static int command_internal_false(anshell& anshell, const std::vector<std::string>& arguments)
{
  anshell.environment_get().set("?","1");
  return 1;
}

static int command_internal_export(anshell& anshell, const std::vector<std::string>& arguments)
{
  for (size_t i = 0; i < arguments.size(); i++ ) {
    anshell.environment_get().export_var(arguments[i]);
  }
  anshell.environment_get().set("?","0");
  return 0; 
}

void
command_internal::init()
{
  commands["?"] = &command_internal_help;
  commands["cd"] = &command_internal_cd;
  commands["exit"] = &command_internal_exit;
  commands["export"] = &command_internal_export;
  commands["false"] = &command_internal_false;
  commands["help"] = &command_internal_help;
  commands["path"] = &command_internal_path;
  commands["pwd"] = &command_internal_pwd;
  commands["type"] = &command_internal_type;
  commands["true"] = &command_internal_true;
}
